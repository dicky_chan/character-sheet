import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StickyModule } from 'ng2-sticky-kit';
import { AngularDraggableModule } from 'angular2-draggable';

import { AppComponent } from './app.component';
import { CharacterPageComponent } from './character-page/character-page.component';
import { PuzzleGameComponent } from './puzzle-game/puzzle-game.component';

@NgModule({
  declarations: [
    AppComponent,
    CharacterPageComponent,
    PuzzleGameComponent
  ],
  imports: [
    BrowserModule,
    AngularDraggableModule,
	StickyModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
