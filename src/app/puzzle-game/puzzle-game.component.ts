import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-puzzle-game',
  templateUrl: './puzzle-game.component.html',
  styleUrls: ['./puzzle-game.component.css']
})
export class PuzzleGameComponent implements OnInit {

	grid: Cell[][] = [];
	gridObjects: GridObject[] = [];
	GRID_CELL_SIZE: number = 30;
	oldClientPosition: Position = {
		x: 0,
		y: 0
	};

  constructor() { }

  ngOnInit() {
		let GRID_WIDTH: number = 22;
		let GRID_HEIGHT: number = 16;

  	this.grid = [];

  	for (let i = 0; i < GRID_HEIGHT; i++) {
  		this.grid[i] = [];
  		for (let j = 0; j < GRID_WIDTH; j++) {
				this.grid[i][j] = {
					placeable: true,
					occupied: 0
				}; 
  		}
  	}

  	this.grid[0][0].placeable = false;
  	this.grid[0][GRID_WIDTH-1].placeable = false;
  	this.grid[GRID_HEIGHT-1][0].placeable = false;
  	this.grid[GRID_HEIGHT-1][GRID_WIDTH-1].placeable = false;

  	// Generate grid table via jQuery
  	let gameBoard = $('#game-board-grid');
  	let $rowTr = $('<tr _ngcontent-c1></tr>');
  	let $cellTd = $('<td _ngcontent-c1></td>');
  	for (let row of this.grid) {
  		let nRow = $rowTr.clone();
  		for (let cell of row) {
  			let nCell = $cellTd.clone();
  			if (cell.placeable) {
  				nCell.addClass('game-board-grid-cell');
  			}
  			nRow.append(nCell);
  		}
  		gameBoard.append(nRow);
  	}

  	this.gridObjects.push(new GridObject([[1,0,0,0],[1,1,1,1],[1,0,0,0],[1,1,1,1],[1,0,0,0],[1,1,1,1],[1,0,0,0],[1,1,1,1]], 'puzzle-game-obj-01.png'));

  	// Generate grid object via jQuery
  	let $gameObjectDiv = $('<div class="game-grid-object" _ngcontent-c1 tabindex="0"></div>');
  	let $gameBoardGridWrapper = $('#game-board-grid-wrapper');
  	for (let gridObject of this.gridObjects) {
  		let ngo = $gameObjectDiv.clone();
  		ngo.width(gridObject.width());
  		ngo.height(gridObject.height());
  		ngo.css('background-image', gridObject.background());

  		$gameBoardGridWrapper.append(ngo);
  	}
  }

  public onMouseDownGridObject(event:any, o:GridObject) {
  		console.log('Mouse Down');
  	this.oldClientPosition.x = event.clientX;
  	this.oldClientPosition.y = event.clientY;
  	o.oldRenderPosition.x = o.renderPosition.x;
  	o.oldRenderPosition.y = o.renderPosition.y;
  	o.isMoving = true;
  }

  public onMouseMoveGridObject(event:any, o:GridObject) {
  	if (o.isMoving) {
  		console.log('Mouse Moving');
  		o.renderPosition.x = o.oldRenderPosition.x + event.clientX - this.oldClientPosition.x;
  		o.renderPosition.y = o.oldRenderPosition.y + event.clientY - this.oldClientPosition.y;

  		console.log(o.renderPosition);
  		console.log(o.oldRenderPosition);
  		console.log(event.clientX);
  		console.log(this.oldClientPosition);
  		console.log(o.oldRenderPosition.x + event.clientX - this.oldClientPosition.x);

			$(event.target).css('transform', o.transform());
  	}
  }

  public onMouseLeaveOrUpGridObject(event:any, o:GridObject) {
  	if (o.isMoving) {
  		let object = $(event.target);
  		console.log('Mouse Up/Leave');
  		console.log('=======================================');
	  	o.isMoving = false;
	  	let pos = $(event.target).position();
	  	o.position.x = Math.round(pos.left / this.GRID_CELL_SIZE);
	  	o.position.y = Math.round(pos.top / this.GRID_CELL_SIZE);
	  	o.renderPosition.x = this.GRID_CELL_SIZE * o.position.x;
	  	o.renderPosition.y = this.GRID_CELL_SIZE * o.position.y;

			object.css('transform', o.transform());
			object.blur();
  	}
  }

  public onSpaceGridObject(event:any, o:GridObject) {
  	o.rotate += 1
  	o.rotate %= 4;

  	this.onMouseLeaveOrUpGridObject(event, o);
  	this.onMouseDownGridObject(event, o);
  }
}

interface Cell {
	placeable: boolean;
	occupied: number;
}

class GridObject {
	cells: Cell[][];
	rotate: number = 0; //0 = Normal, 1 = 90deg, 2 = 180deg, 3 = 270deg
	image: string = '';

	// Rendering
	position: Position = {
		x: 0,
		y: 0
	}
	original: Position = {
		x: 0,
		y: 0
	}
	renderPosition: Position = {
		x: 0,
		y: 0
	}
	oldRenderPosition: Position = {
		x: 0,
		y: 0
	}
	isMoving:boolean = false;

	public width() {
		return this.cells ? this.cells[0].length * this.GRID_CELL_SIZE : 0;
	}

	public height() {
		return this.cells ? this.cells.length * this.GRID_CELL_SIZE : 0;
	}

	public background() {
		return 'url(\'../../assets/' + this.image + '\')';
	}

	public transform() {
		let rotate = this.rotate * 90;
		let left = this.renderPosition.x;
		let top = this.renderPosition.y;
		return 'translate(' + left + 'px, ' + top + 'px) rotate(' + rotate + 'deg)';
	}

	constructor(cells:number[][], image: string, private GRID_CELL_SIZE = 30) {
		this.cells = [];
		for (let row of cells) {
			let nRow = [];
			for (let cell of row) {
				nRow.push({
					placeable: true,
					occupied: cell
				});
			}
			this.cells.push(nRow);
		}

		this.image = image;
	}
}

interface Position {
	x: number;
	y: number;
}